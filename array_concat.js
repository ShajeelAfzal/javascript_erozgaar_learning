/**
	How to do Concatination with Strings in JS	
*/

var firstArray = new Array("Technical");
var secondArray = new Array("Non-Tech", "Creative");

var courses = firstArray.concat(secondArray);

document.write(courses);